﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace npap.monitoring.model.mechatronparks
{
    public class MountingSystemDBContext:DbContext
    {

        public MountingSystemDBContext()
            : base("MechParkDB")
        {
            Database.SetInitializer<MountingSystemDBContext>(new MechatronTrackerDBInitialiser());
            
        }
        public DbSet<MountingSystem.MountingArray> MountingArrays { get; set; }
        public DbSet<MountingSystem.MountingSystemGeneric> MountingSystemGenerics { get; set; }
        public DbSet<MountingSystem.MountingSystemType> MountingSystemTypes{ get; set; }
        public DbSet<MountingSystem.MountingSystemOrientation> MountingSystemOrientations { get; set; }

    }
}
