﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace npap.monitoring.model.mechatronparks.MountingSystem
{
    public class MountingSystemOrientation
    {
        [Key, Column(Order = 0)]
        public DateTime Time { get; set; }
        [Key, Column(Order=1)]
        public int MountingSystemGenericID { get; set; }

        public virtual MountingSystemGeneric MountingSystemGeneric {get;set;}
        public double Elevation { get; set; }
        public double Azimuth { get; set; }
    }
}
