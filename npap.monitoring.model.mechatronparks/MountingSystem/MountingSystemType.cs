﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npap.monitoring.model.mechatronparks.MountingSystem
{
    public class MountingSystemType
    {
        public int MountingSystemTypeID { get; set; }
        public string Name { get; set; }
        public bool? AzimuthChanges { get; set; }
        public bool? ElevationChanges{ get; set; }

    }
}
