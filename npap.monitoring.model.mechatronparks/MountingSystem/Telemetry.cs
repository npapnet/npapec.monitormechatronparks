﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace npap.monitoring.model.mechatronparks.MountingSystem
{
    [ComplexType]
    public class Telemetry
    {
        public bool hasTelemetry { get; set; }
        public string IPasString { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
