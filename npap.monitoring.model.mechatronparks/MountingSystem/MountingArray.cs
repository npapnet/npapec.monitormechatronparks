﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npap.monitoring.model.mechatronparks.MountingSystem
{
    public class MountingArray
    {
        public int MountingArrayID { get; set; }
        public string Name { get; set; }
        public bool EnableUpdate { get; set; }
        public Telemetry Telemetry { get; set; }
        public virtual List<MountingSystemGeneric> MountingSystemGenerics { get; set; }
    }
}
