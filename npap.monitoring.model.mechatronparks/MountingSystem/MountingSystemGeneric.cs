﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npap.monitoring.model.mechatronparks.MountingSystem
{
    public class MountingSystemGeneric
    {
        public int MountingSystemGenericID { get; set; }
        public int InternalID { get; set; }
        public MountingSystemType MountingSystemType { get; set; }
        public virtual List<MountingSystemOrientation> MountingSystemOrientations { get; set; }
        public virtual MountingArray MountingArray { get; set; }
    }
}
