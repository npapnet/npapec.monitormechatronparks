﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npapEC.Monitor_Mechatron_Parks
{
   public class TrackerMechatron
    {
       public int TrackerID;
       public List<MechatronTrackerState> lstTrackerStates;

       public TrackerMechatron(int TrackerID)
       {
           this.TrackerID = TrackerID;
           lstTrackerStates= new  List<MechatronTrackerState>(); 
       }

       public void AddNewTrackerState(MechatronTrackerState thisState)
       {
           MechatronTrackerState lastTrackerState = null;
           try
           {
               lastTrackerState = lstTrackerStates.Last<MechatronTrackerState>();
               if (lastTrackerState.StateTime.Hour > thisState.StateTime.Hour)
               {
                   lstTrackerStates = new List<MechatronTrackerState>();
                   OnThresholdReached(EventArgs.Empty);
               }
           }
           catch (NullReferenceException)
           {

           }
           lstTrackerStates.Add(thisState);
       }


       #region Event related
       protected virtual void OnThresholdReached(EventArgs e)
       {
           EventHandler handler = LstStateReset;
           if (handler != null)
           {
               handler(this, e);
               System.Diagnostics.Trace.WriteLine("Reseting StateList");
           }
       }

       public event EventHandler LstStateReset; 
       #endregion

    }
}
