﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace npapEC.Monitor_Mechatron_Parks
{
    public enum TrackerMode
    {
    
    }
    public class MechatronTrackerState
    {
        public DateTime StateTime { get; set; }
        public int Mode { get; set; }
        public double Elevation { get; set; }
        public double Azimuth { get; set; }
        public int ElevationPulses { get; set; }
        public int AzimuthPulses { get; set; }

        public MechatronTrackerState(DateTime eventTime, int Mode, double ElevationAngle, double AzimuthAngle, int ElevationPulses, int AzimuthPulses)
        {
            this.Azimuth = AzimuthAngle;
            this.AzimuthPulses = AzimuthPulses;
            this.Elevation = ElevationAngle;
            this.ElevationPulses = ElevationPulses;
            this.StateTime = eventTime;
            this.Mode = Mode;
        }


    }
}
