﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace npapEC.Monitor_Mechatron_Parks
{
    public class MechatronPark:npap.monitoring.model.mechatronparks.MountingSystem.MountingArray
    {
        #region Variable*****************************************

        public bool ParkCommunicationState { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public int NoOfTrackers { get; set; }
        public int HorizontalPark { get; set; }
        public int AlarmTrackers { get; set; }
        public int TrackersWithoutComms { get; set; }


            
        /*Current Park State*/


        /*Local Variables*/
        private Uri uriZData { get; set; }
        private Uri uriTestCGI { get; set; }
        private NetworkCredential _MechatronCredentials = new NetworkCredential("admin", "atlassolar.gr"); //This line ensures the request is processed through Basic Authentication
        private HttpWebRequest _WebReq;
        private HttpWebResponse _WebResp;

        #endregion Variables

        public MechatronPark(string urlAsString, int port)
        {
            this.Name = urlAsString + ":"+port.ToString();
            this.Telemetry = new npap.monitoring.model.mechatronparks.MountingSystem.Telemetry() 
                { 
                    IPasString = urlAsString, 
                    Port=port,
                    hasTelemetry = true, 
                    Password = "atlassolar.gr",
                    Username="admin" };
            this.uriZData = new UriBuilder("http", urlAsString, port, "admin/data.zhtml").Uri;
            this.uriTestCGI = new UriBuilder("http", urlAsString, port, "test.cgi").Uri; 
            this.StatusCode = HttpStatusCode.NoContent;
        }

        /// <summary>
        /// Refreshed internal variables
        /// </summary>
        public void RefreshPark()
        {
            string response = this.getResponse();
            if (response != String.Empty)
            {
                string[] lines = response.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                int temp;
                int.TryParse(lines[0].Split(':')[1], out temp);
                this.NoOfTrackers = temp;

                int.TryParse(lines[1].Split(':')[1], out temp);
                this.HorizontalPark = temp;

                int.TryParse(lines[2].Split(':')[1], out temp);
                this.AlarmTrackers = temp;

                int.TryParse(lines[3].Split(':')[1], out temp);
                this.TrackersWithoutComms = temp;
            }
        }

        public string getResponse()
        {
            //Initialization
            _WebReq = (HttpWebRequest)WebRequest.Create(string.Format(this.uriZData.AbsoluteUri));
            _WebReq.Credentials = _MechatronCredentials;
            _WebReq.Method = "GET";
            _WebResp = (HttpWebResponse)_WebReq.GetResponse();
            //Let's show some information about the response
            this.StatusCode =_WebResp.StatusCode;
            Console.WriteLine(_WebResp.StatusCode);
            Console.WriteLine(_WebResp.Server);
            
            //Now, we read the response (the string), and output it.
            StreamReader _Answer = new StreamReader(_WebResp.GetResponseStream());

            string answerString = _Answer.ReadToEnd();
            Console.WriteLine (answerString);
            return answerString;
        }
        #region TestCgi relevant Code

        public string getTestCgiresponse(string Parameters)
        {
            string str = this.HttpmyPost(Parameters);
            return str;
        }



        /// <summary>
        /// Function for obtaining testCgi data from AtlasSolar webbox
        /// </summary>
        /// <param name="Parameters"></param>
        /// <returns></returns>
        private string HttpmyPost(string Parameters)
        {
            ServicePointManager.UseNagleAlgorithm = false;
            string str = "No response";
            bool completed = false;
            int i = 0; while (completed == false && i < 100)
            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriTestCGI); ;

                //request.Credentials = _MechatronCredentials;
                request.Method = "POST";
                request.Accept = "*/*";
                request.Headers["Cache-Control"] = "no-cache";
                request.Headers["Pragma"] = "no-cache";
                request.Headers.Add("X-Requested-With", "XMLHttpRequest");
                /*request.UserAgent="Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0" ;
                request.Headers.Add("Accept-Language", "en-US,en;q=0.5");
                 request.AutomaticDecompression = DecompressionMethods.GZip|DecompressionMethods.Deflate;
                 request.Referer = "http://" + this.Telemetry.IPasString + ":" + this.Telemetry.Port.ToString() + "/admin/index.zhtml";
                 request.AllowAutoRedirect = true;            
                 //request.TransferEncoding = "";
                 request.Expect = "";
                    
                 request.PreAuthenticate = true;*/
                request.ServicePoint.Expect100Continue = false;
                request.KeepAlive = true;
                request.ProtocolVersion = HttpVersion.Version11;
                request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                byte[] bytes = Encoding.UTF8.GetBytes(Parameters);
                request.ContentLength = bytes.Length;
                try
                {
                    i++;

                    Stream requestStream = request.GetRequestStream();

                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Flush();
                    requestStream.Close();
                    WebResponse response = request.GetResponse();
                    Stream stream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(stream);
                    str = reader.ReadToEnd();
                    stream.Dispose();
                    response.Close();
                    reader.Dispose();
                    completed = true;

                }
                catch (WebException ex)
                {
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                    System.Diagnostics.Trace.WriteLine("retry:" + i.ToString() + ":" + ex.Message);

                }
            }
            return str;
        }

        /// <summary>
        /// Parses AtlasSolar testCgi output 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        internal List<MechatronTrackerState> ParseMechatronTestCgi(string pHTML)
        {
            string pBody=extractHtmlBody(pHTML);
            //
            // Split string on spaces.
            // ... This will separate all the words.
            //
            List<MechatronTrackerState> lstMechatronWebData = new List<MechatronTrackerState>();

            string[] words = pBody.Split(new string[] { "#tblLastStatus >" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string word in words)
            {
                //System.Windows.Forms.MessageBox.Show(word);
                lstMechatronWebData.Add(parseMechatronLine(word));
            }
            return lstMechatronWebData;
        }

        /// <summary>
        /// Parses  a Line from Mechatron Web data.
        /// </summary>
        /// <param name="line"></param>
        /// <returns>MechatronWebData structure which contains all relevant data</returns>
        private MechatronTrackerState parseMechatronLine(string line)
        {
            System.Text.StringBuilder b = new System.Text.StringBuilder(line);
            MechatronTrackerState tracker = new MechatronTrackerState();

            //tbody:last||12 (0),17:59:07,30/01/14 17:56:34,3,1, 241.67,   8.30,280,75,Tracking,18:03:12%%
            b.Replace("tbody:last||", "");
            b.Replace("%%", "");
            string[] data = b.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] strtmp = data[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            tracker.TrackerID = int.Parse(strtmp[0]);
            tracker.TrackerTime = Convert.ToDateTime(data[2]);
            tracker.Tzone = int.Parse(data[3]);
            tracker.Mode = int.Parse(data[4]);
            tracker.Azimuth = double.Parse(data[5]);
            tracker.Elevation = double.Parse(data[6]);
            tracker.PulsesAzimuth = int.Parse(data[7]);
            tracker.PulsesElevation = int.Parse(data[8]);
            tracker.Status = (data[9].Trim());
            tracker.NextTracking = Convert.ToDateTime(data[10]);

            //System.Windows.Forms.MessageBox.Show(line +Environment.NewLine + b.ToString());
            return tracker;
        }
        
        /// <summary>
        /// STructure for carrying Mechatron tracker state
        /// </summary>
        internal struct MechatronTrackerState
        {
            [DisplayName("ID")]
            public int TrackerID{get;set;}
            public double Azimuth { get; set; }
            public double Elevation { get; set; }
            public int Mode { get; set; }
            public DateTime TrackerTime;
            public int Tzone;
            public int PulsesAzimuth;
            public int PulsesElevation;
            public string Status;
            public DateTime NextTracking;

        }

        #endregion
        /// <summary>
        /// extract Html body
        /// </summary>
        /// <param name="html"></param>
        /// <returns>the body within a valid HTML page</returns>
        public static string extractHtmlBody(string html)
        {
            string theBody = null;

            RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
            Regex regx = new Regex("<body>(?<theBody>.*)</body>", options);

            Match match = regx.Match(html);

            if (match.Success)
            {
                theBody = match.Groups["theBody"].Value;
            }
            return theBody;
        }
    }
}