﻿namespace npapEC.Monitor_Mechatron_Parks
{
    partial class frmMainMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainMonitor));
            this.btnLoadParks = new System.Windows.Forms.Button();
            this.bthRefreshPark = new System.Windows.Forms.Button();
            this.dgvParks = new System.Windows.Forms.DataGridView();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.btnShowSingleParkForm = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLoadParks
            // 
            this.btnLoadParks.Location = new System.Drawing.Point(3, 3);
            this.btnLoadParks.Name = "btnLoadParks";
            this.btnLoadParks.Size = new System.Drawing.Size(81, 43);
            this.btnLoadParks.TabIndex = 0;
            this.btnLoadParks.Text = "Reload Parks from Disk";
            this.btnLoadParks.UseVisualStyleBackColor = true;
            // 
            // bthRefreshPark
            // 
            this.bthRefreshPark.Location = new System.Drawing.Point(3, 52);
            this.bthRefreshPark.Name = "bthRefreshPark";
            this.bthRefreshPark.Size = new System.Drawing.Size(81, 43);
            this.bthRefreshPark.TabIndex = 1;
            this.bthRefreshPark.Text = "Refresh Parks";
            this.bthRefreshPark.UseVisualStyleBackColor = true;
            this.bthRefreshPark.Click += new System.EventHandler(this.bthRefreshPark_Click);
            // 
            // dgvParks
            // 
            this.dgvParks.AllowUserToAddRows = false;
            this.dgvParks.AllowUserToDeleteRows = false;
            this.dgvParks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvParks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvParks.Location = new System.Drawing.Point(0, 0);
            this.dgvParks.Name = "dgvParks";
            this.dgvParks.ReadOnly = true;
            this.dgvParks.Size = new System.Drawing.Size(688, 225);
            this.dgvParks.TabIndex = 2;
            this.dgvParks.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvParks_CellContentClick);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.dgvParks);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.btnShowSingleParkForm);
            this.splitContainer.Panel2.Controls.Add(this.textBox1);
            this.splitContainer.Panel2.Controls.Add(this.bthRefreshPark);
            this.splitContainer.Panel2.Controls.Add(this.btnLoadParks);
            this.splitContainer.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer_Panel2_Paint);
            this.splitContainer.Size = new System.Drawing.Size(688, 345);
            this.splitContainer.SplitterDistance = 225;
            this.splitContainer.TabIndex = 3;
            // 
            // btnShowSingleParkForm
            // 
            this.btnShowSingleParkForm.Location = new System.Drawing.Point(90, 3);
            this.btnShowSingleParkForm.Name = "btnShowSingleParkForm";
            this.btnShowSingleParkForm.Size = new System.Drawing.Size(94, 43);
            this.btnShowSingleParkForm.TabIndex = 4;
            this.btnShowSingleParkForm.Text = "Open Single Park";
            this.btnShowSingleParkForm.UseVisualStyleBackColor = true;
            this.btnShowSingleParkForm.Click += new System.EventHandler(this.btnShowSingleParkForm_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(209, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(467, 92);
            this.textBox1.TabIndex = 3;
            // 
            // frmMainMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 345);
            this.Controls.Add(this.splitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMainMonitor";
            this.Text = "Main Monitoring Form";
            this.Load += new System.EventHandler(this.frmMainMonitor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvParks)).EndInit();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLoadParks;
        private System.Windows.Forms.Button bthRefreshPark;
        private System.Windows.Forms.DataGridView dgvParks;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnShowSingleParkForm;
    }
}

