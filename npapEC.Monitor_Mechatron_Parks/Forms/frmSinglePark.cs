﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using npap.monitoring.model.mechatronparks.MountingSystem;

namespace npapEC.Monitor_Mechatron_Parks.Forms
{
	public partial class frmSinglePark : Form
	{
		#region Variable Declaration***************************

		public npap.monitoring.model.mechatronparks.MountingSystem.MountingArray Park = null;
		private MechatronPark MPark = null;
		private List<MechatronPark.MechatronTrackerState> lst;
		private Timer tmr = new Timer();

		#endregion Variable Declaration

		#region Constructor***************************

		public frmSinglePark()
		{
			InitializeComponent();
		}

		private void frmSinglePark_Load(object sender, EventArgs e)
		{
			tmr.Interval = 30 * 60 * 1000; // 30 min in [ms]
			tmr.Tick += tmr_Tick;
		}

		private void frmSinglePark_FormClosing(object sender, FormClosingEventArgs e)
		{
			this.Visible = false;
			e.Cancel = true;
		}

		#endregion Constructor
		#region Methods ***************************

		private void CheckIfParkObjectIsCurrent()
		{
			if (MPark == null)
			{
				MPark = new MechatronPark(Park.Telemetry.IPasString, Park.Telemetry.Port);
			} else if (Park.Name != MPark.Name)
			{
				MPark = new MechatronPark(Park.Telemetry.IPasString, Park.Telemetry.Port);
			}

		}


		/// <summary>
		/// adds the data to the database
		/// </summary>
		private void AddToDatabase()
		{
			if (MPark.ParkCommunicationState == true)
			{
				using (var context = new npap.monitoring.model.mechatronparks.MountingSystemDBContext())
				{
					var query = from c in context.MountingArrays
								where c.Telemetry.IPasString == "aggelakisgerania1.dyndns.org"
								select c;
					MountingArray mntArray = query.FirstOrDefault();
					foreach (MechatronPark.MechatronTrackerState mts in lst)
					{
						MountingSystemOrientation mso = new MountingSystemOrientation() { Azimuth = mts.Azimuth, Elevation = mts.Elevation, Time = mts.TrackerTime };
						var qu2 = from tr in mntArray.MountingSystemGenerics
								  where tr.InternalID == mts.TrackerID
								  select tr;
						qu2.FirstOrDefault().MountingSystemOrientations.Add(mso);
					}

					try
					{
						context.SaveChanges();
					}
					catch (Exception)
					{
						//throw;
					}
				}
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("There is no communication");
			}
		}

		internal void ResetParameters()
		{
			CheckIfParkObjectIsCurrent();
			tbNoOfTrackers.Text = MPark.NoOfTrackers.ToString();
			//TODO: tbLastUpdate
			tbIPAdress.Text = Park.Name;
		}

		private void PerformParkPerformanceUpdate()
		{
			tbHttpOutput.Text = MPark.getTestCgiresponse("cmd=21&prm=0");
			try
			{
				lst = MPark.ParseMechatronTestCgi(tbHttpOutput.Text);
				var bindingList = lst;
				var source = new BindingSource(bindingList, null);
				dataGridView1.DataSource = source;
				//set autosize mode
				dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
				dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
				dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
				dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
				dataGridView1.Refresh();
				MPark.ParkCommunicationState = true;
			}
			catch (Exception)
			{
				System.Diagnostics.Debug.WriteLine("No communication from " + Park.Name);
				MPark.ParkCommunicationState = false;
			}
		}

		/// <summary>
		/// support function that may be use dto initialise the database through teh seed method.
		/// </summary>
		private void AuxInitDatabase()
		{
			List<string> lstString;
			using (var context = new npap.monitoring.model.mechatronparks.MountingSystemDBContext())
			{
				var query = from c in context.MountingSystemTypes
							select c.Name;
				lstString = query.ToList();
			}

			foreach (string s in lstString)
			{
				tbHttpOutput.Text += Environment.NewLine + s;
			}
		}

		#endregion
		#region Control Event Handlers ***************************
		private void tmr_Tick(object sender, EventArgs e)
		{
			PerformParkPerformanceUpdate();
			AddToDatabase();
		}


		private void btnRefreshParkState_Click(object sender, EventArgs e)
		{
			PerformParkPerformanceUpdate();
			AddToDatabase();
		}
		private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
		}
		private void chkbxAutoUpdate_CheckedChanged(object sender, EventArgs e)
		{
			if (chkbxAutoUpdate.Checked)
			{
				PerformParkPerformanceUpdate();
				AddToDatabase();
				tmr.Start();
			}
			else { tmr.Stop(); }
		} 
		#endregion

        private void btnRefreshGraph_Click(object sender, EventArgs e)
        {

            chrtTracker.Series.Clear();

            List<int> _lstSiteIn = new List<int>();
            if (dataGridView1.SelectedRows.Count == 0)
            {//Select all 
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    _lstSiteIn.Add((int)row.Cells[0].Value);
                }
            }
            else
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    _lstSiteIn.Add((int)row.Cells[0].Value);
                }
            }
            using (var context = new npap.monitoring.model.mechatronparks.MountingSystemDBContext())
            {
                foreach (int i in _lstSiteIn)
                {
                    chrtTracker.Series.Add(PrepareTrackerSeries(context, i));
                }
            }

        }

        /// <summary>
        /// Prepares an individual Data Series from inverter data
        /// </summary>
        /// <param name="context"></param>
        /// <param name="SiteInverterID"></param>
        /// <param name="DateTimeRange"></param>
        /// <param name="PowerFlag"></param>
        /// <returns></returns>
        private Series PrepareTrackerSeries(npap.monitoring.model.mechatronparks.MountingSystemDBContext context, int SiteInverterID)//, SelectionRange DateTimeRange, bool PowerFlag)
        {
            Series tmpSeries = new Series();

            tmpSeries.ChartType = SeriesChartType.Line;
            //using (var context = new PVParkMonitoringContext())
            //{
            npap.monitoring.model.mechatronparks.MountingSystem.MountingSystemGeneric thisSiteInverter 
                    = context.MountingSystemGenerics.Find(SiteInverterID);
            var query = from c in thisSiteInverter.MountingSystemOrientations
                   /*.Where(c => (c.ReportTime.Date >= DateTimeRange.Start.Date &&
                    c.ReportTime.Date <= DateTimeRange.End.Date))*/
                        select new { Time = c.Time, Azimuth= (double)c.Azimuth, Elevation = (double) c.Elevation};//, Dew = (double)c.DewPoint };

            tmpSeries.Name = "Tracker" + SiteInverterID.ToString() + " CumEnergy [kWh]";
            foreach (var c in query)
            {
                tmpSeries.Points.AddXY(c.Time, c.Azimuth);
                //DewPointSeries.Points.AddXY(c.Time, c.Dew);
            }
            //}

            return tmpSeries;
        }


	}
}