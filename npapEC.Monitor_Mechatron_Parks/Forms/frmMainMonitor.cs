﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using npapEC.Monitor_Mechatron_Parks.Forms;

namespace npapEC.Monitor_Mechatron_Parks
{
    public partial class frmMainMonitor : Form
    {
        List<npap.monitoring.model.mechatronparks.MountingSystem.MountingArray> Parks = null;
        frmSinglePark frmSPrk= new frmSinglePark();

        public frmMainMonitor()
        {
            InitializeComponent();
            ReloadParksFromDatabase();

        }

        private void ReloadParksFromDatabase()
        {
            using (var context = new npap.monitoring.model.mechatronparks.MountingSystemDBContext())
            {
                Parks = context.MountingArrays.ToList<npap.monitoring.model.mechatronparks.MountingSystem.MountingArray>();
            }
            dgvParks.DataSource = Parks;
        }

        private void btnRefreshParks_Click(object sender, EventArgs e)
        {
            ReloadParksFromDatabase();
        }

        private void bthRefreshPark_Click(object sender, EventArgs e)
        {
            try
            {
                bthRefreshPark.Enabled = false;
                bthRefreshPark.Text = "Refreshing";
                if (Parks != null)
                {
                    foreach (MechatronPark MP in Parks)
                    {
                        bthRefreshPark.Text = "Refreshing " + MP.Name;
                        MP.RefreshPark();
                        /*                      
                         MessageBox.Show("Park IP" + MP.IPAddress +
                            "\nNo Of Trackers:" + MP.NoOfTrackers +
                            "\nHoriz.Park:" + MP.HorizontalPark +
                            "\nAlarm:" + MP.AlarmTrackers +
                            "\nNo Comms:" + MP.TrackesWithoutComms
                            );*/
                    }
                    bthRefreshPark.Text = "Refresh parks ";
                }
            }
            catch { }
            finally
            {
                //dgvParks.DataSource = Parks;
                dgvParks.Refresh();
                bthRefreshPark.Enabled = true;
                bthRefreshPark.Text = "Refresh Park";
            }

            
        }

        private void btnGetPositionData_Click(object sender, EventArgs e)
        {
            MechatronPark mechPark = new MechatronPark("aggelakisgerania1.dyndns.org", 8002);
            //mechPark = new MechatronPark("ngmenergy.dyndns.org", 8058);
            string str = 
                 mechPark.getTestCgiresponse("cmd=21&prm=0");
            textBox1.Text = str;
        }



        private void ShowSingleParkForm()
        {

            frmSPrk.Visible = !frmSPrk.Visible;
            if (frmSPrk.Visible)
            {
                npap.monitoring.model.mechatronparks.MountingSystem.MountingArray SelectedPark = Parks[dgvParks.CurrentCell.RowIndex];
                frmSPrk.Park = SelectedPark;
                frmSPrk.ResetParameters();
            }
        }

        private void btnShowSingleParkForm_Click(object sender, EventArgs e)
        {
            ShowSingleParkForm();
        }

        private void dgvParks_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            ShowSingleParkForm();
        }

        private void frmMainMonitor_Load(object sender, EventArgs e)
        {

        }

        private void splitContainer_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }



 

    }
}
