﻿namespace npapEC.Monitor_Mechatron_Parks.Forms
{
    partial class frmSinglePark
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnRefreshParkState = new System.Windows.Forms.Button();
            this.tbNoOfTrackers = new System.Windows.Forms.TextBox();
            this.tbLastUpdate = new System.Windows.Forms.TextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.tbIPAdress = new System.Windows.Forms.TextBox();
            this.tbHttpOutput = new System.Windows.Forms.TextBox();
            this.chkbxAutoUpdate = new System.Windows.Forms.CheckBox();
            this.chrtTracker = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnRefreshGraph = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrtTracker)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(293, 150);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btnRefreshParkState
            // 
            this.btnRefreshParkState.Location = new System.Drawing.Point(12, 179);
            this.btnRefreshParkState.Name = "btnRefreshParkState";
            this.btnRefreshParkState.Size = new System.Drawing.Size(122, 45);
            this.btnRefreshParkState.TabIndex = 1;
            this.btnRefreshParkState.Text = "Refresh Park State";
            this.btnRefreshParkState.UseVisualStyleBackColor = true;
            this.btnRefreshParkState.Click += new System.EventHandler(this.btnRefreshParkState_Click);
            // 
            // tbNoOfTrackers
            // 
            this.tbNoOfTrackers.Location = new System.Drawing.Point(311, 12);
            this.tbNoOfTrackers.Name = "tbNoOfTrackers";
            this.tbNoOfTrackers.ReadOnly = true;
            this.tbNoOfTrackers.Size = new System.Drawing.Size(100, 20);
            this.tbNoOfTrackers.TabIndex = 2;
            this.tbNoOfTrackers.Text = "No of Trackers";
            // 
            // tbLastUpdate
            // 
            this.tbLastUpdate.Location = new System.Drawing.Point(311, 38);
            this.tbLastUpdate.Name = "tbLastUpdate";
            this.tbLastUpdate.ReadOnly = true;
            this.tbLastUpdate.Size = new System.Drawing.Size(100, 20);
            this.tbLastUpdate.TabIndex = 3;
            this.tbLastUpdate.Text = "LastUpdate";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(471, 12);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(121, 97);
            this.treeView1.TabIndex = 4;
            // 
            // tbIPAdress
            // 
            this.tbIPAdress.Location = new System.Drawing.Point(311, 64);
            this.tbIPAdress.Name = "tbIPAdress";
            this.tbIPAdress.ReadOnly = true;
            this.tbIPAdress.Size = new System.Drawing.Size(154, 20);
            this.tbIPAdress.TabIndex = 2;
            this.tbIPAdress.Text = "IPAddress";
            // 
            // tbHttpOutput
            // 
            this.tbHttpOutput.Location = new System.Drawing.Point(311, 115);
            this.tbHttpOutput.Multiline = true;
            this.tbHttpOutput.Name = "tbHttpOutput";
            this.tbHttpOutput.ReadOnly = true;
            this.tbHttpOutput.Size = new System.Drawing.Size(221, 51);
            this.tbHttpOutput.TabIndex = 2;
            this.tbHttpOutput.Text = "Output";
            // 
            // chkbxAutoUpdate
            // 
            this.chkbxAutoUpdate.AutoSize = true;
            this.chkbxAutoUpdate.Location = new System.Drawing.Point(311, 179);
            this.chkbxAutoUpdate.Name = "chkbxAutoUpdate";
            this.chkbxAutoUpdate.Size = new System.Drawing.Size(116, 17);
            this.chkbxAutoUpdate.TabIndex = 5;
            this.chkbxAutoUpdate.Text = "Automatic Updates";
            this.chkbxAutoUpdate.UseVisualStyleBackColor = true;
            this.chkbxAutoUpdate.CheckedChanged += new System.EventHandler(this.chkbxAutoUpdate_CheckedChanged);
            // 
            // chrtTracker
            // 
            chartArea1.Name = "ChartArea1";
            this.chrtTracker.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chrtTracker.Legends.Add(legend1);
            this.chrtTracker.Location = new System.Drawing.Point(12, 230);
            this.chrtTracker.Name = "chrtTracker";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chrtTracker.Series.Add(series1);
            this.chrtTracker.Size = new System.Drawing.Size(574, 249);
            this.chrtTracker.TabIndex = 6;
            this.chrtTracker.Text = "chart1";
            // 
            // btnRefreshGraph
            // 
            this.btnRefreshGraph.Location = new System.Drawing.Point(471, 179);
            this.btnRefreshGraph.Name = "btnRefreshGraph";
            this.btnRefreshGraph.Size = new System.Drawing.Size(115, 45);
            this.btnRefreshGraph.TabIndex = 7;
            this.btnRefreshGraph.Text = "Graph ";
            this.btnRefreshGraph.UseVisualStyleBackColor = true;
            this.btnRefreshGraph.Click += new System.EventHandler(this.btnRefreshGraph_Click);
            // 
            // frmSinglePark
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 500);
            this.Controls.Add(this.btnRefreshGraph);
            this.Controls.Add(this.chrtTracker);
            this.Controls.Add(this.chkbxAutoUpdate);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.tbLastUpdate);
            this.Controls.Add(this.tbHttpOutput);
            this.Controls.Add(this.tbIPAdress);
            this.Controls.Add(this.tbNoOfTrackers);
            this.Controls.Add(this.btnRefreshParkState);
            this.Controls.Add(this.dataGridView1);
            this.Name = "frmSinglePark";
            this.Text = "Single Mechatron Park";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSinglePark_FormClosing);
            this.Load += new System.EventHandler(this.frmSinglePark_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrtTracker)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnRefreshParkState;
        private System.Windows.Forms.TextBox tbNoOfTrackers;
        private System.Windows.Forms.TextBox tbLastUpdate;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.TextBox tbIPAdress;
        private System.Windows.Forms.TextBox tbHttpOutput;
        private System.Windows.Forms.CheckBox chkbxAutoUpdate;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrtTracker;
        private System.Windows.Forms.Button btnRefreshGraph;
    }
}