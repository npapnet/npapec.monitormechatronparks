﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace npapEC.Monitor_Mechatron_Parks.Resources
{
    public class AuxilliaryFunctions
    {
        /// <summary>
        /// Loads Park IP addresses from the relevant file. 
        /// </summary>
        /// <returns></returns>
        public static List<MechatronPark> LoadMechatronParksFromDisk()
        {
            List<MechatronPark> Parks = new List<MechatronPark>();
            string result = GetDataFromParksIPFiles();
            // Break up each line
            using (StringReader reader = new StringReader(result))
            {
                string line = string.Empty;
                do
                {
                    line = reader.ReadLine();
                    if (line != null)
                    {
                        if (line.Substring(0, 1) == "#")
                        {
                            // what happens if there is an ignore character. 
                        }
                        else 
                        {
                            string[] data= line.Split(':');
                            string IPAddress = data[0];
                            int Port = int.Parse(data[1]);
                            Parks.Add(new MechatronPark(IPAddress,Port) );
                        }
                    }

                } while (line != null);
            }
            return Parks;
        }

        private static string GetDataFromParksIPFiles()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "npapEC.Monitor_Mechatron_Parks.Resources.ParkIPAddresses.txt";
            string result;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }
            return result;
        }

        
    }
}
