﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using npapEC.Monitor_Mechatron_Parks;

namespace UTMechatronParksMonitoring
{
    [TestClass]
    public class UnitTest1
    {
        string litStr = @"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD W3 HTML//EN"">
<HTML><HEAD></HEAD><BODY>#tblLastStatus > tbody:last||1 (0),17:59:06,30/01/14 17:55:59,3,1, 241.59,   8.39,258,74,Tracking

,17:56:57%%#tblLastStatus > tbody:last||2 (0),17:59:06,30/01/14 17:58:04,3,1, 241.91,   8.03,261,75,Tracking

,18:07:50%%#tblLastStatus > tbody:last||3 (0),17:59:06,30/01/14 17:59:09,3,1, 242.07,   7.84,261,75,Tracking

,18:07:56%%#tblLastStatus > tbody:last||4 (0),17:59:06,30/01/14 17:57:40,3,1, 241.83,   8.12,281,74,Tracking

,18:02:52%%#tblLastStatus > tbody:last||5 (0),17:59:06,30/01/14 17:59:00,3,1, 242.05,   7.87,280,75,Tracking

,18:02:44%%#tblLastStatus > tbody:last||6 (0),17:59:06,30/01/14 17:58:37,3,1, 241.95,   7.98,273,75,Tracking

,18:01:09%%#tblLastStatus > tbody:last||7 (0),17:59:06,30/01/14 17:59:43,3,1, 242.12,   7.79,273,74,Tracking

,17:59:58%%#tblLastStatus > tbody:last||8 (0),17:59:06,30/01/14 17:59:09,3,1, 242.08,   7.84,256,75,Tracking

,18:06:27%%#tblLastStatus > tbody:last||9 (0),17:59:06,30/01/14 17:59:21,3,1, 242.07,   7.85,265,74,Tracking

,17:59:34%%#tblLastStatus > tbody:last||10 (0),17:59:07,30/01/14 17:56:39,3,1, 241.70,   8.27,264,75,Tracking

,17:59:35%%#tblLastStatus > tbody:last||11 (0),17:59:07,30/01/14 17:56:34,3,1, 241.68,   8.29,265,75,Tracking

,18:00:07%%#tblLastStatus > tbody:last||12 (0),17:59:07,30/01/14 17:56:34,3,1, 241.67,   8.30,280,75,Tracking

,18:03:12%%</BODY></HTML>";

        [TestMethod]
        public void CreateANewMechatronPark()
        {
            MechatronPark mechPark = new MechatronPark("aggelakisgerania1.dyndns.org", 8002);
            Assert.AreEqual("aggelakisgerania1.dyndns.org:8002", mechPark.Name);
            Assert.AreEqual("aggelakisgerania1.dyndns.org", mechPark.Telemetry);
            Assert.AreEqual(8002, mechPark.Telemetry.Port);
        }

        [TestMethod]
        public void Test2ServerCommunicationStatus()
        {
            MechatronPark mechPark = new MechatronPark("aggelakisgerania1.dyndns.org", 8002);
            string getVars = "";
            string str = mechPark.getResponse();
            Assert.AreEqual("aggelakisgerania1.dyndns.org:8002", mechPark.Name);
            Assert.AreEqual("Tracking", str.Substring(0, 8));
        }

        [TestMethod]
        public void Test2LoadMechatronParksFromDisk()
        {
            List<MechatronPark>  Parks = new List<MechatronPark>();
            Parks = npapEC.Monitor_Mechatron_Parks.Resources.AuxilliaryFunctions.LoadMechatronParksFromDisk();
            Assert.AreEqual(1, Parks.Count);  // Number of loaded parks
        }

        [TestMethod]
        public void TesttheTestCgiResponse()
        {
            MechatronPark //mechPark = new MechatronPark("ngmenergy.dyndns.org", 8058);
            mechPark = new MechatronPark("aggelakisgerania1.dyndns.org", 8002);
            string str = mechPark.getTestCgiresponse("cmd=21&prm=0");
            System.Windows.Forms.MessageBox.Show(str);
            Assert.AreNotEqual("No response", str);
        }


        [TestMethod]
        public void TestDateTimeChange()
        {
            System.DateTime myDateTime1 = new DateTime(2013, 12, 09, 22,23,25);
            System.DateTime myDateTime2 = new DateTime(2013, 12, 10, 22, 23, 25);
        }


        [TestMethod]
        public void TestRegEx()
        {
            //System.Windows.Forms.MessageBox.Show(extractHtmlBody(litStr));
            MechatronPark mechPark = new MechatronPark("aggelakisgerania1.dyndns.org", 8002);
            
             
             Assert.AreEqual(12, 12);
        }



        #region  transfer to other 

        #endregion
    }
}